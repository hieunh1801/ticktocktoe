## Summary
1. Component lifecyle

2. Props, state
3. Pass props từ cha -> con và từ con -> cha
4. Stateless component và statefull component
5. highorder component. DOM

6. Cho react-native
- react-native navigation
- custom native module
- advanced: viết thư viện cho react-native 

7. Sử dụng các view cơ bản
7.1 Sử dụng các UI kit có sẵn : native base, react-native element, react-native material ..

#### 1. Component lifecyle
- Khi một component được viết dưới dạng class kế thừa từ __React.Component__.
- __React.Component__ có một số phương thức chung "lifecyle method" được gọi vào các thời điểm khác nhau trong vòng đời của một component. Gồm 
- __Mounting :__ được gọi khi mà một component được khởi tạo và insert vào DOM 
- __Updating__
- __Unmounting__
- __ErrorHandling__