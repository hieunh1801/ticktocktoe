import React from "react";
import Board from "./Board";
import "./styles.css";
export default class Game extends React.Component {
    
    constructor(props) {
        super(props) 
        let arrayDefault = [
            ["", "", "", ""],
            ["", "", "", ""],
            ["", "", "", ""],
            ["", "", "", ""],
        ]
        this.state = {
            history : [
                {
                    arraySize: 4,
                    squares : arrayDefault,
                },
            ],
            isXnext: true,
            stepNumber: 0,
        }
        console.log("Constructor: this.state", this.state)
    }
    
    handleClick(i) {
        let history = this.state.history.slice(0, this.state.stepNumber + 1)
        let currentIndexOfHistory = this.state.history.length - 1;
        let squares = this.state.history[currentIndexOfHistory].squares;
        let size = this.state.history[currentIndexOfHistory].arraySize;
        let newSize = size;
        let newSquares = squares.slice();

        ;
        // TH2: Điền rồi or Win
        if(squares[ i[0] ][ i[1] ] || calculateWinner(squares)) {
            console.log("Winner is", calculateWinner(squares))
            return;
        }
        
        // TH2: Chưa điền thì sẽ cho điền và SetState để hiển thị lại bàn cờ
        newSquares[ i[0] ][ i[1] ] = this.state.isXnext ? "X" : "O";
        
        
        // Khi mà điền ra biên thì sẽ mở rộng bàn cờ
        if( i[0] === size - 1 || i[1] === size -1|| i[0] === 0 || i[1] === 0 ){
            // Create mang 2 chieu
            newSize = size + 8
            newSquares = createNewArray(squares);
            this.setState({
                history: history.concat({
                        arraySize: newSize,
                        squares: newSquares,
                }),
                
                
                

            })
            return
        } else {
            this.setState({
                history: history.concat({
                    arraySize: newSize,
                    squares: newSquares
                }),
                isXnext: !this.state.isXnext,
                stepNumber: history.length,
            })
            return
        }
    }
    jumpTo(step) {
        this.setState({
            stepNumber: step,
            xIsNext: (step%2) === 0
        })
    }
    render() {
        let history = this.state.history
        let currentIndexOfHistory = this.state.history.length - 1;
        // curent square
        let squares = this.state.history[this.state.stepNumber].squares.slice();
        
        // console.log(squares)
        let size = this.state.history[currentIndexOfHistory].arraySize;
        let winner = calculateWinner(squares);
        const maxStep = history.length;
        
        const moves = history.map((step, move) => {
            const desc = move ?
            'Go to move #' + move :
            'Go to game start';
          return (
            <li key={move}>
              <button onClick={() => this.jumpTo(move)}>{desc}</button>
            </li>
          );
        })

        let status;
        if (winner) {
            status = "Winner: " + winner;
          } else {
            status = "Next player: " + (this.state.xIsNext ? "X" : "O");
          }
        console.log("Render Squares: " , squares);
        console.log("Render this.state: " , this.state);
        return (
            <div className="game">
                <div className="boardContainer">
                    <Board 
                        squares={squares}
                        size = {size}
                        onClick={(i) => this.handleClick(i)}
                    />
                </div>

                <div className="infomation">
                        {status}
                    {/* Show Jumback To step list */}
                    <ol>{moves}</ol> 
                    </div>
            </div>
        );
    }
}

function createNewArray(array) {
    let newArray = Array();
    let size = array[0].length;
    let newSize = size + 8;
    let insert4 = Array(4).fill("");
    let insertRowOldArray = Array(size).fill("");
    for(let j = 0; j < newSize; j++) {
        if(j < 4 || j> newSize - 5) {
            // newArray[j] = [...insert4, ...insertRowOldArray, ...insert4 ]
            newArray[j] = insert4.concat(insertRowOldArray).concat(insert4)
        }
        if(j >= 4 && j<= newSize - 5) {
            // newArray[j] = [...insert4, ...array[j - 4], ...insert4]
            newArray[j] = insert4.concat(array[j - 4]).concat(insert4);
        }
        
    }
    // let row= Array(size+8).fill("");
    // newArray = Array(size + 8).fill(row);
    return newArray
}   

function calculateWinner(squares) {
    
    let i,j;
    let size = squares[0].length;
    // squares = createNewArray(squares);
    console.log("calculateWinner - squares: ",squares)
    console.log("calculateWinner - size:", size);
    for(i=0; i<size-3; i++) {
        for(j=0; j<size-3; j++)
        {

            if(squares[i][j] && squares[i][j] === squares[i+1][j+1] && squares[i][j] === squares[i+2][j+2]) {
                console.log("Winner")
                return squares[i][j];
            }            
            if(squares[i][j] &&squares[i][j] === squares[i+1][j] && squares[i][j] === squares[i+2][j]) {
                console.log("Winner")
                return squares[i][j];
            }            
            if(squares[i][j] &&squares[i][j] === squares[i][j+1] && squares[i][j] === squares[i][j+2]) {
                console.log("Winner")
                return squares[i][j];
            }            
               
            
        }
    }
    return null;
  
}