import React from 'react';
import "./styles.css";
export default function Square(props) {
    return (
        <button 
            className="square"
            onClick = {() => 
            {
                console.log("Square called onClicked");
                return props.onClick();
            }}>
           {props.value}
        </button>
    )
} 