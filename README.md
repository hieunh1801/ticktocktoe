# Game Tick-tock-toe
https://reactjs.org/tutorial/tutorial.html

# Compoent needed
- Square : là một button để click vào
- Board : Là bảng hiển thị xem có bao nhiêu ô
- Game: Show ra board. Sau này click vào sẽ thay đổi.

#### Truyền dữ liệu thông qua props

#### Tìm hiểu thêm
- Lyce Cycle bản mới nhất
- Redux
- Redux - Saga
- Webpack - đóng gói sản phẩm, sẽ làm một số công việc tiền xử lý trước khi đẩy lên server
- package.json
- Constructor(props, context)
- return nhiều hơn một thành phần

# 29 - 01 - 2019
#### Immutability
- __Discuss :__ Khi data thay đổi ta có 2 cách tiếp cận sau để giải quyết
```js
    // Cách 1: Thay đổi trực tiếp dữ liệu
    let player = {name: "Hieu", score: 98}
    payler.score = 100; // thay đổi một cách trực tiếp
    
    // Cách 2: Lưu giá trị vào một biến mới, tạo ra một biến mới lưu giá trị mới
    let newPlayer = {... player, score: 100}

    // => 2 kết quả nhìn có vẻ giống . Tuy nhiên
    // - Cách 1: Làm thay đổi trực tiếp giá trị của biến. => mutability
    // - Cách 2: Tạo ra một biến mới lưu trữ gía trị mới. => Immutability

    // Đối với array ta làm cách sau
    var score = [1,2,3,4,5,6,7,8,10]
    let newScore = score.slice(); 
    newScore[1] = 12;
```
- Lợi ích của việc không làm đột biến data (__Immutable__) :
    + __Complex feature become easier__ : jump back to one or more step previous. Từ đó lưu trữ nguyên vẹn các giá trị từ các version cũ => sau này tái sử dụng lại được. __=>__ Từ đó nên tránh việc thay đổi dữ liệu một cách trực tiếp để sau này còn tái sử dụng chúng nó.
    + __Detecting Changes:__ Việc phát hiện sự thay đổi khi ta thay đổi dữ liệu một cách trực tiếp (__mutable__) là vô cùng khó (__nguyên nhân:__ ta thay đổi rồi còn gì để so sánh với nó nữa). Nếu làm theo __Immutable__ thì ta còn có cái cũ để tham chiếu tới => easy 
    + __Determining When to Re-render in React :__ Xác định xem khi nào cần re-render lại component từ đó cải thiện hiệu năng app. Component có nên update không?  https://reactjs.org/docs/optimizing-performance.html#examples

#### Function Component - Khuyến khích viết Component dạng function. 
__WHY__ 

|Function Component|Component extends React.Component|
|:-|:-|
|Giống hệt function bình thường, props hiểu giống như parameter thông thường|Component giống như một class => kế thừa từ Component cha của thằng React - tức là sẽ kế thừa __constructor__, __componentwillmount__, __componentdidmont()__,  và __State__ ...|
|Buil thẳng luôn, nhanh gọn trực tiếp|rõ ràng chậm hơn nhiều|

# 30 - 01 -2019

### Picking a Key - List

- Khi mà ta render ra một list, React sẽ lưu trữ một vài thông tin về mỗi item trong list. Khi chúng ta update một list, React cần phải xác định cái gì đã thay đổi. Do đó chúng ta có thể add, removed, re-arranged, update list'items.

```js
// Ví dụ:
    <li>Alexa: 7 tasks left</li>
    <li>Ben: 5 tasks left</li>
// biến đổi thành
    <li>Ben: 9 tasks left</li>
    <li>Claudia: 8 tasks left</li>
    <li>Alexa: 5 tasks left</li>

/*
    Khi mọi người nhìn vào sẽ thấy:
    => Số lượng thay đổi
    - Alexa swap với Ben
    - Chèn Claudia vào giữa Alexa và Ben
*/

```
- __=>__ Vấn đề là làm sao để React hiểu chúng ta có ý định gì. Bởi vì React không hiểu ý định của chúng ta, chúng ta cần phải chỉ định __key__ property cho mỗi item trong list để phân biệt các item với nhau.  
- Khi muốn hiển thị dữ liệu từ Database - Alexa, Ben, Claudia id có thể sử dụng như key.
```js
    // Chỉ định key cho các list item như sau:
    // sử dụng property key trong li
    <li key={user.id}> 
        {user.name} : {user.taskCount}
    </li>
```
- __Điều gì sẽ xảy ra khi React render list :__ React lấy mỗi key từ item trong list và so sánh chúng với list trước đó để so khớp key. Sẽ xảy ra các trường hợp sau.
    + Nếu list-mới mà có một key mới chưa tồn tại trước đó => React tạo ra một component
    + Nếu list-mới mất đi một key đã tồn tại trong list-cũ thì React sẽ hủy component trước đó. 
    + Nếu 2 key khớp với nhau, component tương ứng sẽ bị di chuyển. 
- Key sẽ giúp cho react xách định danh tính của mỗi component, điều này cho phép react duy trì được state giữa các lần re-render. Khi mà một key bị thay đổi, component sẽ bị hủy và re-created với new state.

- __key__ rất đặc biệt và là property kín đáo trong React (kết hợp với __ref__ sẽ làm công việc trở nên dễ dàng hơn). Khi một phần tử được tạo ra, React trích xuất __key__ property và lưu trữ trực tiếp trong returned element. __Mặc dù key__ trông có vẻ là một phần của __props__ tuy nhiên ta không thể tham chiếu tới nó như sau __this.props.key__. React sẽ tự động sử dụng key để quyết định xem component nào update. __Một component không thể xem key của chính nó__

-__It’s strongly recommended that you assign proper keys whenever you build dynamic lists.__ Nếu không có key thích hợp nên nghĩ tới việc tái cấu trúc lại dữ liệu

- __Nếu không chỉ định key cụ thể__ React sẽ cảnh báo và sử dụng một array index như là key theo mặc đinh. Tuy nhiên không khuyến khích sử dụng trường hợp này vì việc cố gắng insert sort removing sẽ trở nên khó khăn hơn.

### Pure, stateless component 
### == và ===

# 31-01-2018
# TICKTOCK TOE Advanced
- Mở rộng trang khi click ra biên
- Vẫn phải giữ được những chức năng cũ

    - Bước 1: Tạo history và đẩy ra các màn hình tại các thời điểm khác nhau
        - 1.1 : Tọa ra một ô
        - 1.2 : Tọa ra Board có props là x và y là mảng 2 chiều.
            - data.map() để return ra list component
    - Bước 2: Mỗi khi clicked ra biên => 
        - 2.1 : Mở rộng mảng. 
            + Ý tưởng: tạo một mảng mới đã mở rộng ra 4 chiều __OK__
            + Điền các phần tử cũ vào.__OK__
            + gọi đến this.setState() để cập nhật các thay đổi __OK__
        - 2.2 : Thay đổi chỉ số của các phần tử cũ __OK__
    - Bước 3: Điền OX vào bàn cờ __OK__
    - Bước 3: Kiểm tra điều kiên thắng trong cờ Caro __OK__
    - Bước 4: Tạo Frame bọc lại bàn cờ __Chưa OK lắm__
    - Bước 4: Tạo các bước step next, step back __OK__
