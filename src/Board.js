import React, { Component } from 'react';

import Square from "./Square";
import "./styles.css";

export default class Board extends Component {

	render() {
		let squares = this.props.squares;
		// let size = this.props.size;
		
		// console.log("squares" , squares)


		// render Board
		const listItems = squares.map((row, idRow) => 
			<div
				key={idRow.toString()} 
				className="row"
			>
				{row.map((item, idItemInRow) => 
						<Square
							key={(idRow.toString()+" "+idItemInRow.toString())}
							className="square" 
							onClick={() => {
								console.log("Board called onClick", idRow, idItemInRow)
								return	this.props.onClick([idRow, idItemInRow])
							}}
							value = {this.props.squares[idRow][idItemInRow]}>
								{item}
						</Square>
					) 
				}
			</div>
		)			 

		return(
			<div className="board">
				{
					listItems
				}	
			</div>
		)
		
	}
}
