import React from "react";

import Board from "./Board";
import "./styles.css";

export default class Game extends React.Component {
    
    constructor(props) {
        super(props) 
        console.log("--------------------------Game Constructor--------------------------")
        const initialSize = 6;
        const row = Array(initialSize).fill("");
        const initialSquares = Array(initialSize).fill(row);
        this.state = {
            history : [
                {
                    squares : initialSquares,
                },
            ],
            isXnext: true,
            stepNumber: 0,
        }
        console.log("Constructor: this.state", this.state.history)
    }
    
    handleClick(i) {
        console.log("-----------------------------Game - handleClick(i)---------------------------------")
        // Cắt ra và map ra vùng nhớ mới cho history
        let history = this.state.history.slice(0, this.state.stepNumber + 1).map(squares => ({...squares}))
        let current = history[history.length - 1];

        let squares = current.squares.map(row => [...row]);
        let size = squares[0].length;
        
        // bàn cờ mới hiện vẫn đang bằng bàn cờ cũ
        let newSquares = [];

        // TH2: Điền rồi or Win
        if(squares[ i[0] ][ i[1] ] || calculateWinner(squares)) {
            console.log("Winner is", calculateWinner(squares))
            return;
        }
        
        // TH2: Chưa điền thì sẽ cho điền và SetState để hiển thị lại bàn cờ
        squares[i[0]][i[1]] = this.state.isXnext ? "X" : "O";
        
        
        // Khi mà điền ra biên thì sẽ mở rộng bàn cờ
        if( i[0] === size - 1 || i[1] === size -1|| i[0] === 0 || i[1] === 0 ){
            // Create mang 2 chieu
            newSquares = createNewArray(squares);
            this.setState({
                history: history.concat(
                    {  
                        squares: newSquares,
                    }
                ),
                stepNumber: history.length,
                isXnext: !this.state.isXnext
            })
            return
        } else {
            newSquares = squares.map(row => [...row]);
            this.setState({
                history: history.concat(
                    {
                        squares: newSquares
                    }
                ),
                stepNumber: history.length,
                isXnext: !this.state.isXnext,
                
            })
            return
        }
    }

    jumpTo(step) {

        this.setState({
            stepNumber: step,
            xIsNext: (step%2) === 0
        })
    }

    render() {
        console.log("-----------------------------Game - Render()---------------------------------")
        // Copy deep Array object _______________________________________
        const history = this.state.history.map(squares => ({...squares}))
        const currentStepNumber = this.state.stepNumber;
        const currentSquare = history[currentStepNumber].squares.slice();
        const size = currentSquare[0].length;
        const maxStep = history.length;

        const winner = calculateWinner(currentSquare);
        const status = winner? ("Winner IS "+ winner): " "
        
        const moves = history.map((step, move) => {
            const desc = move ? "Go to move #" + move : "Go to game start"
            return (
                <li key={move}>
                    <button onClick={() => {this.jumpTo(move)}}>
                        {desc}
                    </button>
                </li>
            )
        
        })
        
        console.log("history", history);
        console.log("currentStepNumber", currentStepNumber);
        console.log("currentSquare", currentSquare);
        console.log("size", size);


        return (
            <div className="game">
                <div className="boardContainer">
                    <Board 
                        squares={currentSquare}
                        size = {size}
                        onClick={(i) => {
                            return this.handleClick(i) 
                        }}
                    />
                </div>

                <div className="infomation">
                    <div className="winner">
                        {status}
                    </div>
                        
                    <div>
                        <button onClick={() => {
                                if(!this.state.stepNumber) {
                                    return
                                }
                                return (this.jumpTo(this.state.stepNumber - 1))
                            }}

                        >
                            Previous Step
                        </button>
                    </div>
                    <div>
                        <button onClick={() => {
                                if(this.state.stepNumber >= maxStep - 1) {
                                    return
                                }
                                return(this.jumpTo(this.state.stepNumber + 1))
                            }}>
                            Next Step
                        </button>
                    </div>
                    {/* Show Jumback To step list */}
                    <ol>{moves}</ol> 
                </div>
            </div>
        );
    }
}

/**
 *  Increment size of squares up 4 to top - left - right - bottom
 * @param {*} array 
 */
function createNewArray(squares) {
    console.log("-----------------------------Create New Array---------------------------------")
    let newSquares = [];
    let size = squares[0].length;
    let newSize = size + 8;
    console.log("size", size, "newSize", newSize)

    let insert4 = Array(4).fill("");
    let insertRow = Array(newSize).fill("");
    
    for(let j = 0; j < newSize; j++) {
        if(j < 4 || j> newSize - 5) {
            newSquares[j] = insertRow;
        }
        if(j >= 4 && j<= newSize - 5) {
            newSquares[j] = insert4.concat(squares[j - 4]).concat(insert4);
        }
        
    }
    
    console.log("square: ", squares);
    console.log("newSquare: ", newSquares);
    return newSquares;
}   

/**
 *  Caculator winner of Squares
 * @param {*} squares 
 */
function calculateWinner(squares) {
    console.log("-----------------------------Calculated Winner---------------------------------")
    let i,j;
    let size = squares[0].length;
    console.log("squares: ", squares)
    for(i=0; i<size-2; i++) {
        for(j=0; j<size-2; j++)
        {
            if(squares[i][j] && squares[i][j] === squares[i+1][j+1] && squares[i][j] === squares[i+2][j+2]) {
                return squares[i][j];
            }            
            if(squares[i][j] &&squares[i][j] === squares[i+1][j] && squares[i][j] === squares[i+2][j]) {
                return squares[i][j];
            }            
            if(squares[i][j] &&squares[i][j] === squares[i][j+1] && squares[i][j] === squares[i][j+2]) {
                return squares[i][j];
            }            
            if(i > 1 && j > 1 && squares[i][j] &&squares[i][j] === squares[i-1][j+1] && squares[i][j] === squares[i-2][j+2]) {
                return squares[i][j];
            }
            if(i > 1 && j > 1 && squares[i][j] &&squares[i][j] === squares[i+1][j-1] && squares[i][j] === squares[i+2][j-2]) {
                return squares[i][j];
            }
        }
    }

    return null;
  
}