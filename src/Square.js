import React from 'react';

import "./styles.css";
export default function Square(props) {
    if (props.value === "X" ) {
        return (
            <button 
                className="square valueX" 
                onClick = {() => 
                {
                    console.log("Square called onClicked");
                    return props.onClick();
                }}>
            {props.value}
            </button>
        )
        
    } else {
        return (
            <button 
                className="square valueO" 
                onClick = {() => 
                {
                    console.log("Square called onClicked");
                    return props.onClick();
                }}>
            {props.value}
            </button>
        )
    }
} 